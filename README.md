# js-game

L'objectif de ce projet est de faire un jeu en Javascript. Peu importe le type de jeu.

* Il faudra utiliser des objets JS d'une manière ou d'une autre (pour stocker des valeurs ou autre).
* Il n'est pas nécessaire que le jeu soit responsive (donc si vous devez utiliser du positionnement absolute, c'est pas grave)

La méthode de travail suivante pourra être utilisée :
1. Choisir le type de jeu qu'on veut faire, regarder si c'est pas trop facile ou trop compliqué
2. Maquetter son jeu : faire un ou plusieurs ecrans des éléments fonctionnels qui seront affichés (pas de graphisme ni rien) juste pour avoir une idée de l'interface de votre jeu, des éléments à afficher ou autre
3. (avant ou après la maquette) Lister les différentes fonctionnalités du jeu de manière exhaustive (toutes les actions possibles et ce qui peut se passer fonctionnellement, afin de vous donner une idée de ce qu'il faudra coder)
4. Commencer à coder le système de jeu, sans aucun graphisme ni rien (des div avec des carrés de couleurs ou autre, rien de plus)
5. Faire un développement itératif : fragmenter le travail en plein de petit morceau, faire fonctionnalité par fonctionnalité, ne pas tout faire à la fois (genre si je fais le déplacement, je fais pas les actions tant que j'ai pas terminé un truc qui se déplace de manière à peu près satisfaisante)
6. Pourquoi pas se définir plusieurs version du jeu "une v1 où il sera possible de faire ça et ça, une v2 où j'ajoute ci et ça etc."

